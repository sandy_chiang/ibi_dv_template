# README #

This is a dev bootstrap for building / testing WebFOCUS extensions without the need for having
WebFOCUS installed.

# what's in it? #

This contains all the core objects that the WebFOCUS extension API provides

to help build out an extension.

This includes:

1. Example set of data
2. Example metadata (dataBucket)
3. Tooltip info

