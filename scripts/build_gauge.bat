rmdir ..\build\com.ibi.gauge /s /q
mkdir ..\build\com.ibi.gauge /s /q
xcopy ..\com.ibi.gauge\css ..\build\com.ibi.gauge\css  /E /H /C /I
xcopy ..\com.ibi.gauge\icons ..\build\com.ibi.gauge\icons  /E /H /C /I
xcopy ..\com.ibi.gauge\lib ..\build\com.ibi.gauge\lib  /E /H /C /I
xcopy ..\com.ibi.gauge\com.ibi.gauge.js ..\build\com.ibi.gauge\com.ibi.gauge.js*
xcopy ..\com.ibi.gauge\properties.json ..\build\com.ibi.gauge\properties.json*
xcopy ..\com.ibi.gauge\license.txt ..\build\com.ibi.gauge\license.txt*
xcopy ..\com.ibi.gauge\README.md ..\build\com.ibi.gauge\README.md*
xcopy ..\com.ibi.gauge\arc_gauge_extension_example.html ..\build\com.ibi.gauge\arc_gauge_extension_example.html*