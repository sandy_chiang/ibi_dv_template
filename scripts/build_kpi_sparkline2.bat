rmdir ..\build\com.ibi.kpi.sparkline2 /s /q
mkdir ..\build\com.ibi.kpi.sparkline2 /s /q
xcopy ..\com.ibi.kpi.sparkline2\css ..\build\com.ibi.kpi.sparkline2\css  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline2\icons ..\build\com.ibi.kpi.sparkline2\icons  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline2\lib ..\build\com.ibi.kpi.sparkline2\lib  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline2\fonts ..\build\com.ibi.kpi.sparkline2\fonts  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline2\com.ibi.kpi.sparkline2.js ..\build\com.ibi.kpi.sparkline2\com.ibi.kpi.sparkline2.js*
xcopy ..\com.ibi.kpi.sparkline2\properties.json ..\build\com.ibi.kpi.sparkline2\properties.json*
xcopy ..\com.ibi.kpi.sparkline2\license.txt ..\build\com.ibi.kpi.sparkline2\license.txt*
xcopy ..\com.ibi.kpi.sparkline2\google_attribution_license.txt ..\com.ibi.kpi.sparkline2\google_attribution_license.txt*
