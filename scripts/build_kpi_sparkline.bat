rmdir ..\build\com.ibi.kpi.sparkline /s /q
mkdir ..\build\com.ibi.kpi.sparkline /s /q
xcopy ..\com.ibi.kpi.sparkline\css ..\build\com.ibi.kpi.sparkline\css  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline\icons ..\build\com.ibi.kpi.sparkline\icons  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline\lib ..\build\com.ibi.kpi.sparkline\lib  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline\fonts ..\build\com.ibi.kpi.sparkline\fonts  /E /H /C /I
xcopy ..\com.ibi.kpi.sparkline\com.ibi.kpi.sparkline.js ..\build\com.ibi.kpi.sparkline\com.ibi.kpi.sparkline.js*
xcopy ..\com.ibi.kpi.sparkline\properties.json ..\build\com.ibi.kpi.sparkline\properties.json*
xcopy ..\com.ibi.kpi.sparkline\license.txt ..\build\com.ibi.kpi.sparkline\license.txt*
xcopy ..\com.ibi.kpi.sparkline\google_attribution_license.txt ..\build\com.ibi.kpi.sparkline\google_attribution_license.txt*
